import os
import re

from setuptools import setup
from subprocess import check_output


def getGitVersion(tagPrefix):
    """Return a version string with information about this git checkout.
    If the checkout is an unmodified, tagged commit, then return the tag version.
    If this is not a tagged commit, return the output of ``git describe --tags``.
    If this checkout has been modified, append "+" to the version.
    """
    path = os.getcwd()
    if not os.path.isdir(os.path.join(path, '.git')):
        return None

    v = check_output(['git', 'describe', '--tags', '--dirty', '--match=%s*' % tagPrefix]).strip().decode('utf-8')

    # chop off prefix
    assert v.startswith(tagPrefix)
    v = v[len(tagPrefix):]

    # split up version parts
    parts = v.split('-')

    # has working tree been modified?
    modified = False
    if parts[-1] == 'dirty':
        modified = True
        parts = parts[:-1]

    # have commits been added on top of last tagged version?
    # (git describe adds -NNN-gXXXXXXX if this is the case)
    local = None
    if len(parts) > 2 and re.match(r'\d+', parts[-2]) and re.match(r'g[0-9a-f]{7}', parts[-1]):
        local = parts[-1]
        parts = parts[:-2]

    gitVersion = '-'.join(parts)
    if local is not None:
        gitVersion += '+' + local
    if modified:
        gitVersion += 'm'

    return gitVersion


setup(name='objecthash',
      version=getGitVersion(""),
      description='Python class to generate hash for python objects using standard hashing functions',
      url='http://github.com/pmaunz/objecthash',
      author='Peter Maunz',
      author_email='peter@yb171.org',
      license='MIT',
      packages=['objecthash'],
      install_requires=[],
      dependency_links=[],
      zip_safe=False)
