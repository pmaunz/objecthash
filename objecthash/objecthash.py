import inspect


class Hasher(object):
    """Hashes Python python object data using supplied hashing function."""

    def __init__(self, digest):
        """Initialize Hasher, digest can be any hashing function that has the same interface as for
        example hashlib.sha256"""
        self._hashobj = digest()

    def update(self, v):
        """Add `v` to the hash, recursively if needed."""
        self._hashobj.update(str(type(v)).encode())
        if isinstance(v, str):
            self._hashobj.update(v.encode())
        elif isinstance(v, (bytes, bytearray)):
            self._hashobj.update(v)
        elif v is None or isinstance(v, (int, float, bool, complex)):
            self._hashobj.update(repr(v).encode())
        elif isinstance(v, (tuple, list)):
            for e in v:
                self.update(e)
        elif isinstance(v, set):
            for e in sorted(v):
                self.update(e)
        elif isinstance(v, dict):
            for key, val in sorted(v.items()):
                self.update(key)
                self.update(val)
        elif isinstance(v, type):
            self._hashobj.update(str(v).encode())
        elif hasattr(v, '__getstate__'):
            self.update(v.__getstate__())
        elif hasattr(v, '__getnewargs_ex__'):
            self.update(v.__getnewargs_ex__())
        elif hasattr(v, '__getnewargs__'):
            self.update(v.__getnewargs__())
        elif hasattr(v, '__reduce_ex__'):
            to_call, *args = v.__reduce_ex__(0)
            self._hashobj.update(to_call.__name__.encode())
            self.update(args)
        elif hasattr(v, '__reduce__'):
            to_call, *args = v.__reduce_ex__(0)
            self._hashobj.update(to_call.__name__.encode())
            self.update(args)
        else:
            for k in sorted(dir(v)):
                if k.startswith('__'):
                    continue
                a = getattr(v, k)
                if inspect.isroutine(a):
                    continue
                self.update(k)
                self.update(a)

    def digest(self):
        """Retrieve the digest of the hash."""
        return self._hashobj.digest()

    def hexdigest(self):
        """Return hexdigest"""
        return self._hashobj.hexdigest()
