objecthash
----------

objectshash creates the hash of a python object. It uses standard hashlib hashing functions and can process most
pickleable objects.

    >>> import objecthash
    >>> import hashlib
    >>> h = objecthash.Hasher(hashlib.sha256)
    >>> h.update({1: 1, 2: 2, 3: 3})
    >>> print(h.hexdigest())